FROM node:12.22.6-alpine AS build

WORKDIR /app
COPY package.json package.json
COPY package-lock.json package-lock.json
RUN yarn install
COPY . .
RUN yarn run build

FROM nginx:1.21.3-alpine
COPY ./config/nginx.conf /etc/nginx/nginx.conf
COPY --from=build app/build opt/site
EXPOSE 80
EXPOSE 443