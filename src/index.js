import React from 'react';
import ReactDOM from 'react-dom';
import {DatePicker} from "antd";
import 'antd/dist/antd.css';

const InitComp = () => {
    return (
        <DatePicker />
    )
}

ReactDOM.render(<InitComp />, document.getElementById('app'))

